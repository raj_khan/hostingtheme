$(document).ready(function() {

    'use strict';

    /* ======= Preloader ======= */
    $(window).on('load', function() {
        $("#preloader").delay(150).fadeOut("slow");
    });

    /* ======= Animation ======= */

    new WOW().init();

    /* ======= Text in banner ======= */
    $('.typist')
        .typist({
            speed: 15,
            text: 'EARCH YOUR DOMAIN',
            cursor: false
        })
        .typistStop();

    /* ======= Testimonials slider ======= */
    $('.client-say').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false
    });


    /* ======= More and less button function in blog page ======= */
    var size_li = $(".post").size();
    var x = 9;
    $('.post:lt(' + x + ')').show();
    $('.moreposts').on('click', function() {
        x = (x + 3 <= size_li) ? x + 3 : size_li;
        $('.post:lt(' + x + ')').show();
        if ($(".post:hidden").length == 0) {
            $(".moreposts").hide();
            $(".lessposts").css("display", "block");
        }
    });
    $('.lessposts').on('click', function() {
        x = (x - 3 < 9) ? 9 : x - 3;
        $('.post').not(':lt(' + x + ')').hide();
        if ($(".post:visible").length == 9 || $(".post:visible").length < 9) {
            $(".lessposts").hide();
            $(".moreposts").css("display", "block");
        }
    });

    /* ======= Filter in team page ======= */
    var $grid = $('.grid').isotope({
        itemSelector: '.element-item',
        layoutMode: 'fitRows'
    });
    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        }, // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };
    // bind filter button click
    $('.filters-button-group').on('click', 'button', function() {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        $grid.isotope({
            filter: filterValue
        });
    });
    // change is-checked class on buttons
    $('.button-group').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });

});