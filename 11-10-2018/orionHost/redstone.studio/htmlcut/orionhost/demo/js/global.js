/*-------------------------------------------------------------------------------------------------------------------------------*/
/*This is main JS file that contains custom style rules used in this template*/
/*-------------------------------------------------------------------------------------------------------------------------------*/
/* Template Name: Site Title*/
/* Version: 1.0 Initial Release*/
/* Build Date: 22-04-2015*/
/* Author: Unbranded*/
/* Website: http://moonart.net.ua/site/ 
/* Copyright: (C) 2015 */
/*-------------------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------*/
/* TABLE OF CONTENTS: */
/*--------------------------------------------------------*/
/* 01 - VARIABLES */
/* 02 - PAGE CALCULATIONS */
/* 03 - FUNCTION ON DOCUMENT READY */
/* 04 - FUNCTION ON PAGE LOAD */
/* 05 - FUNCTION ON PAGE RESIZE */
/* 06 - SWIPER SLIDERS */
/* 07 - BUTTONS, CLICKS, HOVERS */
/* 08 - FORM ELEMENTS - CHECKBOXES AND RADIOBUTTONS */

/*-------------------------------------------------------------------------------------------------------------------------------*/

jQuery(function($) {

	"use strict";

	/*================*/
	/* 01 - VARIABLES */
	/*================*/
	var swipers = [], winW, winH, winScr, _isresponsive, smPoint = 768, mdPoint = 992, lgPoint = 1200, addPoint = 1600, _ismobile = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);

	/*========================*/
	/* 02 - PAGE CALCULATIONS */
	/*========================*/
	function pageCalculations(){
		winW = $(window).width();
		winH = $(window).height();
		if($('.menu-button').is(':visible')) _isresponsive = true;
		else _isresponsive = false;
	}

	/*=================================*/
	/* 03 - FUNCTION ON DOCUMENT READY */
	/*=================================*/
	pageCalculations();	

	/*============================*/
	/* 04 - FUNCTION ON PAGE LOAD */
	/*============================*/
	$(window).on("load", function(){
		$('#loader-wrapper').fadeOut();
		initSwiper();
	});

	/*==============================*/
	/* 05 - FUNCTION ON PAGE RESIZE */
	/*==============================*/
	function resizeCall(){
		pageCalculations();

		var swiperResponsive = $('.swiper-container.initialized[data-slides-per-view="responsive"]');

		for (var i = 0; i < swiperResponsive.length; i++) {
			var thisSwiper = swipers['swiper-'+$(swiperResponsive[i]).attr('id')], $t = $(swiperResponsive[i]), slidesPerViewVar = updateSlidesPerView($t);
			thisSwiper.params.slidesPerView = slidesPerViewVar;
			thisSwiper.reInit();
			var paginationSpan = $t.find('.pagination span');
			var paginationSlice = paginationSpan.hide().slice(0,(paginationSpan.length+1-slidesPerViewVar));
			if(paginationSlice.length<=1 || slidesPerViewVar>=$t.find('.swiper-slide').length) $t.addClass('pagination-hidden');
			else $t.removeClass('pagination-hidden');
			paginationSlice.show();
		};
	}
	if(!_ismobile){
		$(window).resize(function(){
			resizeCall();
		});
	} else{
		window.addEventListener("orientationchange", function() {
			resizeCall();
		}, false);
	}

	/*=====================*/
	/* 06 - SWIPER SLIDERS */
	/*=====================*/
	function initSwiper(){
		var initIterator = 0;

		var swiperContainer = $('.swiper-container');

		for (var i = 0; i < swiperContainer.length; i++) {						  
			var $t = $(swiperContainer[i]);								  

			var index = 'swiper-unique-id-'+initIterator;

			$t.addClass('swiper-'+index + ' initialized').attr('id', index);
			$t.find('.pagination').addClass('pagination-'+index);

			var autoPlayVar = parseInt($t.attr('data-autoplay'),10);
			var centerVar = parseInt($t.attr('data-center'),10);
			var simVar = ($t.closest('.circle-description-slide-box').length)?false:true;

			var slidesPerViewVar = $t.attr('data-slides-per-view');
			if(slidesPerViewVar == 'responsive'){
				slidesPerViewVar = updateSlidesPerView($t);
			}
			else if(slidesPerViewVar!='auto') slidesPerViewVar = parseInt(slidesPerViewVar, 10);

			var loopVar = parseInt($t.attr('data-loop'),10);
			var speedVar = parseInt($t.attr('data-speed'),10);
			var initialSlideVar = parseInt($t.attr('data-initial-slide'),10);
			if(!initialSlideVar){initialSlideVar=0;}

			swipers['swiper-'+index] = new Swiper('.swiper-'+index,{
				speed: speedVar,
				pagination: '.pagination-'+index,
				loop: loopVar,
				paginationClickable: true,
				autoplay: autoPlayVar,
				slidesPerView: slidesPerViewVar,
				keyboardControl: true,
				calculateHeight: true, 
				simulateTouch: simVar,
				centeredSlides: centerVar,
				roundLengths: true,
				initialSlide: initialSlideVar,
				onInit: function(swiper){
					if($t.find('.swiper-copy-block')){
						var qVal = $t.find('.swiper-slide-active').attr('data-val');
						var copyBlock = $t.find('.swiper-slide[data-val="'+qVal+'"] .swiper-copy-block').html();
						$t.find('.swiper-bottom-text').empty().append(copyBlock);
					}
				},
				onSlideChangeEnd: function(swiper){
					var activeIndex = (loopVar===true)?swiper.activeIndex:swiper.activeLoopIndex;
					var qVal = $t.find('.swiper-slide-active').attr('data-val');
					$t.find('.swiper-slide[data-val="'+qVal+'"]').addClass('active');
					if($t.find('.swiper-copy-block')){
						var copyBlock = $t.find('.swiper-slide[data-val="'+activeIndex+'"] .swiper-copy-block').html();
						$t.find('.swiper-bottom-text').empty().append(copyBlock);
					}
				},
				onSlideChangeStart: function(swiper){
					var activeIndex = (loopVar===true)?swiper.activeIndex:swiper.activeLoopIndex;
					$t.find('.swiper-slide.active').removeClass('active');
					$t.find('.icon-pag.active').removeClass('active');
					$t.find('.icon-pag').eq(activeIndex).addClass('active');					
				},
				onSlideClick: function(swiper){

				}
			});
			swipers['swiper-'+index].reInit();
			if($t.attr('data-slides-per-view')=='responsive'){
				var paginationSpan = $t.find('.pagination span');
				var paginationSlice = paginationSpan.hide().slice(0,(paginationSpan.length+1-slidesPerViewVar));
				if(paginationSlice.length<=1 || slidesPerViewVar>=$t.find('.swiper-slide').length) $t.addClass('pagination-hidden');
				else $t.removeClass('pagination-hidden');
				paginationSlice.show();
			}
			initIterator++;
		};

	}

	function updateSlidesPerView(swiperContainer){
		if(winW>=addPoint) return parseInt(swiperContainer.attr('data-add-slides'),10);
		else if(winW>=lgPoint) return parseInt(swiperContainer.attr('data-lg-slides'),10);
		else if(winW>=mdPoint) return parseInt(swiperContainer.attr('data-md-slides'),10);
		else if(winW>=smPoint) return parseInt(swiperContainer.attr('data-sm-slides'),10);
		else return parseInt(swiperContainer.attr('data-xs-slides'),10);
	}

	//swiper arrows
	$('.swiper-arrow-left').on('click', function(){
		swipers['swiper-'+$(this).parent().attr('id')].swipePrev();
	});
	$('.swiper-arrow-right').on('click', function(){
		swipers['swiper-'+$(this).parent().attr('id')].swipeNext();
	});

	//custom arrows
	$('.custom-arrow-left').on('click', function(){
		swipers['swiper-'+$(this).closest('.custom-arrows').find('.swiper-container').attr('id')].swipePrev();
	});
	$('.custom-arrow-right').on('click', function(){
		swipers['swiper-'+$(this).closest('.custom-arrows').find('.swiper-container').attr('id')].swipeNext();
	});

	//icon pagination
	$('.icon-pag').on('click', function(){
		var index = $(this).index();
		swipers['swiper-'+ $(this).parents('.swiper-container').attr('id')].swipeTo(index);
	});	

	/*==============================*/
	/* 07 - BUTTONS, CLICKS, HOVERS */
	/*==============================*/
	$('.tt-header-banner-close').on('click', function(){
		$(this).closest('.tt-header-banner').slideToggle(500);
		$(this).closest('.tt-header').addClass('no-banner');
	});

	//menu
	$('.cmn-toggle-switch').on('click', function(e){
		$(this).toggleClass('active');
		$(this).parents('header').find('.toggle-block').slideToggle();
		e.preventDefault();
	});
	$('.main-nav .menu-toggle').on('click', function(e){
		$(this).closest('li').addClass('select').siblings('.select').removeClass('select');
		$(this).closest('li').siblings('.parent').find('ul').slideUp();
		$(this).parent().siblings('ul').slideToggle();
		e.preventDefault();
	});

	/*accordion*/
	$('.tt-accordion-title').on( 'click', function() {
		if($(this).hasClass('active')){
			$(this).siblings('.tt-accordion-body').slideUp();
			$(this).removeClass('active');
		} else{
			$(this).closest('.tt-accordion').find('.tt-accordion-title.active').removeClass('active');
			$(this).closest('.tt-accordion').find('.tt-accordion-body').slideUp('slow');
			$(this).toggleClass('active');
			$(this).siblings('.tt-accordion-body').slideToggle('slow');
		}
	});

	//Tabs
	var tabFinish = 0;
	$('.tt-nav-tab-item').on('click', function(){		
	    var $t = $(this);
	    if(tabFinish || $t.hasClass('active')) return false;
	    tabFinish = 1;
	    $t.closest('.tt-nav-tab').find('.tt-nav-tab-item').removeClass('active');
	    $t.addClass('active');
	    var index = $t.parent().parent().find('.tt-nav-tab-item').index(this);
	    $t.parents('.tt-tab-nav-wrapper').find('.tt-tab-select select option:eq('+index+')').prop('selected', true);
	    $t.closest('.tt-tab-wrapper').find('.tt-tab-info:visible').fadeOut(500, function(){
	    	var $tabActive  = $t.closest('.tt-tab-wrapper').find('.tt-tab-info').eq(index);
	    	$tabActive.css('display','block').css('opacity','0');
	    	tabReinit($tabActive.parents('.tt-tab-wrapper'));
	    	$tabActive.animate({opacity:1});
	    	tabFinish = 0;
	    });
	});
	$('.tt-tab-select select').on('change', function(){
	    var $t = $(this);
	    if(tabFinish) return false;
	    tabFinish = 1;    
	    var index = $t.find('option').index($(this).find('option:selected'));
	    $t.closest('.tt-tab-nav-wrapper').find('.tt-nav-tab-item').removeClass('active');
	    $t.closest('.tt-tab-nav-wrapper').find('.tt-nav-tab-item:eq('+index+')').addClass('active');
	    $t.closest('.tt-tab-wrapper').find('.tt-tab-info:visible').fadeOut(500, function(){
	    	var $tabActive  = $t.closest('.tt-tab-wrapper').find('.tt-tab-info').eq(index);
	    	$tabActive.css('display','block').css('opacity','0');
	    	tabReinit($tabActive.parents('.tt-tab-wrapper'));
	    	$tabActive.animate({opacity:1});
	    	 tabFinish = 0;
	    });
	});
	function tabReinit($tab){
		var $maps = $tab.find('.map-wrapper');
		for (var i = 0; i < $maps.length; i++) {
			_map.initialize($($maps[i]));	
		}				
	}		
	/*==================================================*/
	/* 08 - FORM ELEMENTS - CHECKBOXES AND RADIOBUTTONS */
	/*==================================================*/

	//theme config - changing color scheme
	$('.theme-config .open').on('click', function(){
		$('.theme-config').toggleClass('active');
	});

	$('.theme-config .colours-wrapper .entry').on('click', function(){
		var prevTheme = $('body').attr('data-theme');
		var newTheme = $(this).attr('data-theme');
		if($(this).hasClass('active')) return false;
		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');
		$('body').attr('data-theme', newTheme);
		$('img').each(function() {
			$(this).attr("src", $(this).attr("src").replace("_th_"+prevTheme, "_th_"+newTheme));
		});		
		localStorage.setItem("theme", newTheme);
	});

	var localStorageThemeVar = localStorage.getItem('theme');
    if (localStorageThemeVar !== null && localStorageThemeVar !== 'null') {
    	$('.theme-config .colours-wrapper .entry[data-theme="'+localStorageThemeVar+'"]').click();
    }
});