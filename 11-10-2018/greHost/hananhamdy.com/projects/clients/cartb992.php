<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from hananhamdy.com/projects/clients/cart.php?a=add&pid=3&language=farsi by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Oct 2018 06:35:53 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>کارت خرید - Grehost</title>

		<!-- Styling -->
<link href="../../../code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

<link href="templates/grehost/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="templates/grehost/css/slick/slick.css" rel="stylesheet">
<link href="templates/grehost/css/slick/slick-theme.css" rel="stylesheet">
<link href="templates/grehost/css/animate.css" rel="stylesheet">
<link href="templates/grehost/css/magnific-popup.css" rel="stylesheet">

<link href="templates/grehost/css/all.minbb08.css?v=5f7f67" rel="stylesheet">
<link href="templates/grehost/css/custom.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript">
    var csrfToken = 'a7ee79e38a77c41780c55d6a594f65d4ad2275b5',
        markdownGuide = 'Markdown Guide',
        locale = 'en',
        saved = 'ذخیره شد',
        saving = 'ذخیره اتوماتیک';
</script>

<script src="templates/grehost/js/scripts.minbb08.js?v=5f7f67"></script>

<script src="templates/grehost/js/jquery-1.12.3.min.js"></script>
<script src="templates/grehost/js/bootstrap.min.js"></script>
<script src="templates/grehost/js/slick.min.js"></script>
<script src="templates/grehost/js/wow.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="templates/grehost/js/jquery.gmap.min.js"></script>
<script src="templates/grehost/js/isotope.pkgd.min.js"></script>
<script src="templates/grehost/js/jquery.magnific-popup.min.js"></script>

<script src="templates/grehost/js/plugins.minbb08.js?v=5f7f67"></script>
<script src="templates/grehost/js/custombb08.js?v=5f7f67"></script>



		

	</head>
	<body>

		

		<!-- ======= Preloader ======= -->
		<div class="preloader">
			<ul class="preloader">
				<li class="one"></li>
				<li class="two"></li>
				<li class="three"></li>
				<li class="four"></li>
				<li class="five"></li>
			</ul>
		</div>
		<!-- ======= Top Header ======= -->
		<div class="top-header">
			<div class="container">
				<ul class="list-inline hidden-xs hidden-sm mini-contact">
					<li>
						<a role="button">+(000)111-222-333</a>
					</li>
					<li>
						<a role="button">someone@example.com</a>
					</li>
				</ul>
				<ul class="list-inline menu">
					<li>
						<!-- Shopping Cart -->
						<div class="pull-right nav">
							<a href="cart14c8.html?a=view" class="quick-nav"><span class="icon fa fa-shopping-cart"></span> <span class="hidden-xs">مشاهده کارت خرید (</span><span id="cartItemCount">0</span><span class="hidden-xs">)</span></a>
						</div>
					</li>
					<li>
						<!-- Login/Account Notifications -->
														</li>
								
								
								<li>
																<div class="pull-right nav">
									<a role="button" class="quick-nav lang" data-toggle="popover" id="languageChooser"><span class="icon fa fa-globe"></span> انتخاب زبان <span class="caret"></span></a>
									<div id="languageChooserContent" class="dropdown hidden">
										<ul class="">
																						<li>
												<a href="cart4b92.html?a=add&amp;pid=3&amp;language=arabic">العربية</a>
											</li>
																						<li>
												<a href="cart37ad.html?a=add&amp;pid=3&amp;language=azerbaijani">Azerbaijani</a>
											</li>
																						<li>
												<a href="cart68c3.html?a=add&amp;pid=3&amp;language=catalan">Català</a>
											</li>
																						<li>
												<a href="cartb6f1.html?a=add&amp;pid=3&amp;language=chinese">中文</a>
											</li>
																						<li>
												<a href="cart81df.html?a=add&amp;pid=3&amp;language=croatian">Hrvatski</a>
											</li>
																						<li>
												<a href="cart766f.html?a=add&amp;pid=3&amp;language=czech">Čeština</a>
											</li>
																						<li>
												<a href="carte998.html?a=add&amp;pid=3&amp;language=danish">Dansk</a>
											</li>
																						<li>
												<a href="cartf0a4.html?a=add&amp;pid=3&amp;language=dutch">Nederlands</a>
											</li>
																						<li>
												<a href="cart1854.html?a=add&amp;pid=3&amp;language=english">English</a>
											</li>
																						<li>
												<a href="cartbdf6.html?a=add&amp;pid=3&amp;language=estonian">Estonian</a>
											</li>
																						<li>
												<a href="cartb992.php?a=add&amp;pid=3&amp;language=farsi">Persian</a>
											</li>
																						<li>
												<a href="cartb2f4.html?a=add&amp;pid=3&amp;language=french">Français</a>
											</li>
																						<li>
												<a href="cart1b3c.html?a=add&amp;pid=3&amp;language=german">Deutsch</a>
											</li>
																						<li>
												<a href="cartf41c.html?a=add&amp;pid=3&amp;language=hebrew">עברית</a>
											</li>
																						<li>
												<a href="cart2f86.html?a=add&amp;pid=3&amp;language=hungarian">Magyar</a>
											</li>
																						<li>
												<a href="cart2346.html?a=add&amp;pid=3&amp;language=italian">Italiano</a>
											</li>
																						<li>
												<a href="cart1b1a.html?a=add&amp;pid=3&amp;language=macedonian">Macedonian</a>
											</li>
																						<li>
												<a href="cart3599.html?a=add&amp;pid=3&amp;language=norwegian">Norwegian</a>
											</li>
																						<li>
												<a href="cart62fe.html?a=add&amp;pid=3&amp;language=portuguese-br">Português</a>
											</li>
																						<li>
												<a href="cartf739.html?a=add&amp;pid=3&amp;language=portuguese-pt">Português</a>
											</li>
																						<li>
												<a href="cart93cb.html?a=add&amp;pid=3&amp;language=romanian">Română</a>
											</li>
																						<li>
												<a href="cart9e80.html?a=add&amp;pid=3&amp;language=russian">Русский</a>
											</li>
																						<li>
												<a href="cartbbc5.html?a=add&amp;pid=3&amp;language=spanish">Español</a>
											</li>
																						<li>
												<a href="cartdd83.html?a=add&amp;pid=3&amp;language=swedish">Svenska</a>
											</li>
																						<li>
												<a href="cartc511.html?a=add&amp;pid=3&amp;language=turkish">Türkçe</a>
											</li>
																						<li>
												<a href="cart53fe.html?a=add&amp;pid=3&amp;language=ukranian">Українська</a>
											</li>
																					</ul>
									</div>
								</div> 					</li>

					
					

					<li class="hidden-xs">
						<div class="pull-right nav">

							<a href="http://hananhamdy.com/projects/grehost/en-layout/index.html" class="main-color">HTML Demo</a>
						</div>
					</li>
					
					<li class="overflow">
											</li>
				</ul>
			</div>
		</div>

		<!-- ======= Header ======= -->
		<header class="navbar">
			<div class="container">
				<a href="index-2.html" class="logo"> <img src="templates/grehost/img/logo.png" alt="Brand" class="img-responsive">
				<div class="logo-txt">
					<b></b>
					<br>
					<span></span>
				</div> </a>
				<!-- ======= Mobile Menu ======= -->
				<a role="button" class="mobile-menu-icon visible-xs visible-sm visible-md"><i class="icon ion-navicon-round"></i></a>

				<!-- ======= Main Menu ======= -->
				<nav class="navbar">
					<div class="" id="myNavbar">
						<ul class="menu list-inline navbar-left">
							    <li menuItemName="Home" id="Primary_Navbar-Home">
        <a href="index-2.html">
                        ناحیه کاربری
                                </a>
            </li>
    <li menuItemName="Store" class="dropdown" id="Primary_Navbar-Store">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Store
                        &nbsp;<b class="caret"></b>        </a>
                    <ul class="dropdown">
                            <li menuItemName="Browse Products Services" id="Primary_Navbar-Store-Browse_Products_Services">
                    <a href="cart.html">
                                                Browse All
                                            </a>
                </li>
                            <li menuItemName="Shop Divider 1" class="nav-divider" id="Primary_Navbar-Store-Shop_Divider_1">
                    <a href="#">
                                                -----
                                            </a>
                </li>
                            <li menuItemName="Shared Hosting" id="Primary_Navbar-Store-Shared_Hosting">
                    <a href="cart1df6.html?gid=1">
                                                Shared Hosting
                                            </a>
                </li>
                            <li menuItemName="Cloude Hosting" id="Primary_Navbar-Store-Cloude_Hosting">
                    <a href="cartba5d.html?gid=2">
                                                Cloude Hosting
                                            </a>
                </li>
                            <li menuItemName="Register a New Domain" id="Primary_Navbar-Store-Register_a_New_Domain">
                    <a href="cart2029.html?a=add&amp;domain=register">
                                                ثبت دامنه جدید
                                            </a>
                </li>
                            <li menuItemName="Transfer a Domain to Us" id="Primary_Navbar-Store-Transfer_a_Domain_to_Us">
                    <a href="cart7c76.html?a=add&amp;domain=transfer">
                                                انتقال دامنه به ما
                                            </a>
                </li>
                        </ul>
            </li>
    <li menuItemName="Announcements" id="Primary_Navbar-Announcements">
        <a href="index992c.html?rp=/announcements">
                        اخبار
                                </a>
            </li>
    <li menuItemName="Knowledgebase" id="Primary_Navbar-Knowledgebase">
        <a href="indexded0.html?rp=/knowledgebase">
                        مرکز آموزش
                                </a>
            </li>
    <li menuItemName="Network Status" id="Primary_Navbar-Network_Status">
        <a href="serverstatus.html">
                        وضعیت شبکه
                                </a>
            </li>
    <li menuItemName="Contact Us" id="Primary_Navbar-Contact_Us">
        <a href="contact.html">
                        تماس با ما
                                </a>
            </li>

						</ul>
						<ul class="menu account-menu list-inline navbar-right">
							    <li menuItemName="Account" class="dropdown" id="Secondary_Navbar-Account">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        حساب
                        &nbsp;<b class="caret"></b>        </a>
                    <ul class="dropdown">
                            <li menuItemName="Login" id="Secondary_Navbar-Account-Login">
                    <a href="clientarea.html">
                                                ورود
                                            </a>
                </li>
                            <li menuItemName="Register" id="Secondary_Navbar-Account-Register">
                    <a href="register.html">
                                                ثبت نام
                                            </a>
                </li>
                            <li menuItemName="Divider" class="nav-divider" id="Secondary_Navbar-Account-Divider">
                    <a href="#">
                                                -----
                                            </a>
                </li>
                            <li menuItemName="Forgot Password?" id="Secondary_Navbar-Account-Forgot_Password?">
                    <a href="pwreset.html">
                                                رمز عبور را فراموش کرده اید؟
                                            </a>
                </li>
                        </ul>
            </li>

						</ul>
					</div>
				</nav>
				
				
				<!-- <nav class="navbar hidden-xs hidden-sm">
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="menu list-inline navbar-left">
							    <li menuItemName="Home" id="Primary_Navbar-Home">
        <a href="/projects/clients/index.php">
                        ناحیه کاربری
                                </a>
            </li>
    <li menuItemName="Store" class="dropdown" id="Primary_Navbar-Store">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Store
                        &nbsp;<b class="caret"></b>        </a>
                    <ul class="dropdown">
                            <li menuItemName="Browse Products Services" id="Primary_Navbar-Store-Browse_Products_Services">
                    <a href="/projects/clients/cart.php">
                                                Browse All
                                            </a>
                </li>
                            <li menuItemName="Shop Divider 1" class="nav-divider" id="Primary_Navbar-Store-Shop_Divider_1">
                    <a href="">
                                                -----
                                            </a>
                </li>
                            <li menuItemName="Shared Hosting" id="Primary_Navbar-Store-Shared_Hosting">
                    <a href="/projects/clients/cart.php?gid=1">
                                                Shared Hosting
                                            </a>
                </li>
                            <li menuItemName="Cloude Hosting" id="Primary_Navbar-Store-Cloude_Hosting">
                    <a href="/projects/clients/cart.php?gid=2">
                                                Cloude Hosting
                                            </a>
                </li>
                            <li menuItemName="Register a New Domain" id="Primary_Navbar-Store-Register_a_New_Domain">
                    <a href="/projects/clients/cart.php?a=add&domain=register">
                                                ثبت دامنه جدید
                                            </a>
                </li>
                            <li menuItemName="Transfer a Domain to Us" id="Primary_Navbar-Store-Transfer_a_Domain_to_Us">
                    <a href="/projects/clients/cart.php?a=add&domain=transfer">
                                                انتقال دامنه به ما
                                            </a>
                </li>
                        </ul>
            </li>
    <li menuItemName="Announcements" id="Primary_Navbar-Announcements">
        <a href="/projects/clients/index.php?rp=/announcements">
                        اخبار
                                </a>
            </li>
    <li menuItemName="Knowledgebase" id="Primary_Navbar-Knowledgebase">
        <a href="/projects/clients/index.php?rp=/knowledgebase">
                        مرکز آموزش
                                </a>
            </li>
    <li menuItemName="Network Status" id="Primary_Navbar-Network_Status">
        <a href="/projects/clients/serverstatus.php">
                        وضعیت شبکه
                                </a>
            </li>
    <li menuItemName="Contact Us" id="Primary_Navbar-Contact_Us">
        <a href="/projects/clients/contact.php">
                        تماس با ما
                                </a>
            </li>

						</ul>
						<ul class="menu account-menu list-inline navbar-right">
							    <li menuItemName="Account" class="dropdown" id="Secondary_Navbar-Account">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        حساب
                        &nbsp;<b class="caret"></b>        </a>
                    <ul class="dropdown">
                            <li menuItemName="Login" id="Secondary_Navbar-Account-Login">
                    <a href="/projects/clients/clientarea.php">
                                                ورود
                                            </a>
                </li>
                            <li menuItemName="Register" id="Secondary_Navbar-Account-Register">
                    <a href="/projects/clients/register.php">
                                                ثبت نام
                                            </a>
                </li>
                            <li menuItemName="Divider" class="nav-divider" id="Secondary_Navbar-Account-Divider">
                    <a href="">
                                                -----
                                            </a>
                </li>
                            <li menuItemName="Forgot Password?" id="Secondary_Navbar-Account-Forgot_Password?">
                    <a href="/projects/clients/pwreset.php">
                                                رمز عبور را فراموش کرده اید؟
                                            </a>
                </li>
                        </ul>
            </li>

						</ul>
					</div>
				</nav> -->
				
				
				
				
			</div>
		</header>

		
		

		<section id="main-body" class="container">

			<div class="row">
								<!-- Container for main page display content -->
				<div class="col-xs-12 main-content">
					
<div style="margin:0 0 10px 0;padding:10px 35px;background-color:#ffffd2;color:#555;font-size:16px;text-align:center;"><strong>Dev License:</strong> This installation of WHMCS is running under a Development License and is not authorized to be used for production use. Please report any cases of abuse to abuse@whmcs.com</div>
<link rel="stylesheet" type="text/css" href="templates/orderforms/standard_cart/css/all.minbb08.css?v=5f7f67" />
<script type="text/javascript" src="templates/orderforms/standard_cart/js/scripts.minbb08.js?v=5f7f67"></script>


<div id="order-standard_cart">

    <div class="row">

        <div class="pull-md-right col-md-9">

            <div class="header-lined">
                <h1>یک دامنه انتخاب کنید...</h1>
            </div>

        </div>

        <div class="col-md-3 pull-md-left sidebar hidden-xs hidden-sm">

                <div menuItemName="Categories" class="panel panel-sidebar">
        <div class="panel-heading">
            <h3 class="panel-title">
                                    <i class="fa fa-shopping-cart"></i>&nbsp;
                
                دسته بندی ها

                
                <i class="fa fa-chevron-up panel-minimise pull-right"></i>
            </h3>
        </div>

        
                    <div class="list-group">
                                                            <a menuItemName="Shared Hosting" href="cart1df6.html?gid=1" class="list-group-item" id="Secondary_Sidebar-Categories-Shared_Hosting">
                            
                            Shared Hosting

                                                    </a>
                                                                                <a menuItemName="Cloude Hosting" href="cartba5d.html?gid=2" class="list-group-item" id="Secondary_Sidebar-Categories-Cloude_Hosting">
                            
                            Cloude Hosting

                                                    </a>
                                                </div>
        
            </div>

        <div menuItemName="Actions" class="panel panel-sidebar">
        <div class="panel-heading">
            <h3 class="panel-title">
                                    <i class="fa fa-plus"></i>&nbsp;
                
                عملیات

                
                <i class="fa fa-chevron-up panel-minimise pull-right"></i>
            </h3>
        </div>

        
                    <div class="list-group">
                                                            <a menuItemName="Domain Registration" href="cart2029.html?a=add&amp;domain=register" class="list-group-item" id="Secondary_Sidebar-Actions-Domain_Registration">
                                                            <i class="fa fa-globe fa-fw"></i>&nbsp;
                            
                            ثبت دامنه جدید

                                                    </a>
                                                                                <a menuItemName="Domain Transfer" href="cart7c76.html?a=add&amp;domain=transfer" class="list-group-item" id="Secondary_Sidebar-Actions-Domain_Transfer">
                                                            <i class="fa fa-share fa-fw"></i>&nbsp;
                            
                            Transfer in a Domain

                                                    </a>
                                                                                <a menuItemName="View Cart" href="cart14c8.html?a=view" class="list-group-item" id="Secondary_Sidebar-Actions-View_Cart">
                                                            <i class="fa fa-shopping-cart fa-fw"></i>&nbsp;
                            
                            مشاهده کارت خرید

                                                    </a>
                                                </div>
        
            </div>

    

        </div>

        <div class="col-md-9 pull-md-right">

            <div class="categories-collapsed visible-xs visible-sm clearfix">

    <div class="pull-left form-inline">
        <form method="get" action="http://hananhamdy.com/projects/clients/cart.php">
            <select name="gid" onchange="submit()" class="form-control">
                <optgroup label="Product Categories">
                                            <option value="1">Shared Hosting</option>
                                            <option value="2">Cloude Hosting</option>
                                    </optgroup>
                <optgroup label="Actions">
                                                                <option value="registerdomain">ثبت دامنه جدید</option>
                                                                <option value="transferdomain">Transfer in a Domain</option>
                                        <option value="viewcart">مشاهده کارت خرید</option>
                </optgroup>
            </select>
        </form>
    </div>

    
</div>


            <form id="frmProductDomain">
                <input type="hidden" id="frmProductDomainPid" value="3" />
                <div class="domain-selection-options">
                                                                <div class="option">
                            <label>
                                <input type="radio" name="domainoption" value="register" id="selregister" checked />ثبت دامنه جدید
                            </label>
                            <div class="domain-input-group clearfix" id="domainregister">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-1">
                                        <div class="row domains-row">
                                            <div class="col-xs-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">www.</span>
                                                    <input type="text" id="registersld" value="" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="لطفا نام دامنه خود را وارد کنید" />
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <select id="registertld" class="form-control">
                                                                                                            <option value=".com">.com</option>
                                                                                                            <option value=".net">.net</option>
                                                                                                            <option value=".org">.org</option>
                                                                                                            <option value=".biz">.biz</option>
                                                                                                            <option value=".info">.info</option>
                                                                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            بررسی
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                                <div class="option">
                            <label>
                                <input type="radio" name="domainoption" value="transfer" id="seltransfer" />انتقال دامنه از شرکت دیگر
                            </label>
                            <div class="domain-input-group clearfix" id="domaintransfer">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-1">
                                        <div class="row domains-row">
                                            <div class="col-xs-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">www.</span>
                                                    <input type="text" id="transfersld" value="" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="لطفا نام دامنه خود را وارد کنید"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <select id="transfertld" class="form-control">
                                                                                                            <option value=".com">.com</option>
                                                                                                            <option value=".net">.net</option>
                                                                                                            <option value=".org">.org</option>
                                                                                                            <option value=".biz">.biz</option>
                                                                                                            <option value=".info">.info</option>
                                                                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            انتقال
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                                <div class="option">
                            <label>
                                <input type="radio" name="domainoption" value="owndomain" id="selowndomain" />از دامنه ای که قبلا ثبت کرده ام استفاده میکنم
                            </label>
                            <div class="domain-input-group clearfix" id="domainowndomain">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="row domains-row">
                                            <div class="col-xs-2 text-right">
                                                <p class="form-control-static">www.</p>
                                            </div>
                                            <div class="col-xs-7">
                                                <input type="text" id="owndomainsld" value="" placeholder="example" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="لطفا نام دامنه خود را وارد کنید" />
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" id="owndomaintld" value="" placeholder="com" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="ضروری" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-block" id="useOwnDomain">
                                            استفاده
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                        </div>

                
            </form>

            <div class="clearfix"></div>
            <form method="post" action="http://hananhamdy.com/projects/clients/cart.php?a=add&amp;pid=3&amp;domainselect=1" id="frmProductDomainSelections">
<input type="hidden" name="token" value="a7ee79e38a77c41780c55d6a594f65d4ad2275b5" />

                <div id="DomainSearchResults" class="hidden">

                    <div id="searchDomainInfo">
                        <p id="primaryLookupSearching" class="domain-lookup-loader domain-lookup-primary-loader domain-searching domain-checker-result-headline">
                            <i class="fa fa-spinner fa-spin"></i>
                            <span class="domain-lookup-register-loader">بررسی در دسترس بودن...</span>
                            <span class="domain-lookup-transfer-loader">تایید روند انتقال دامنه...</span>
                            <span class="domain-lookup-other-loader">تایید نام های انتخابی...</span>
                        </p>
                        <div id="primaryLookupResult" class="domain-lookup-result domain-lookup-primary-results hidden">
                            <div class="domain-unavailable domain-checker-unavailable headline"><strong>:domain</strong> is unavailable</div>
                            <div class="domain-available domain-checker-available headline">تبریک میگوییم! <strong></strong> موجود است!</div>
                            <div class="btn btn-primary domain-contact-support headline">Contact Us</div>
                            <div class="transfer-eligible">
                                <p class="domain-checker-available headline">دامنه شما واجد شرایط انتقال است</p>
                                <p>لطفا قبل از ادامه روند انتقال، از باز بودن قفل انتقال دامنه در رجیستار قبلی، اطمینان حاصل نمایید</p>
                            </div>
                            <div class="transfer-not-eligible">
                                <p class="domain-checker-unavailable headline">روند انتقال قابل انجام نیست</p>
                                <p>دامنه وارد شده، اصلا ثبت نشده است !</p>
                                <p>اگر دامنه اخیرا ثبت شده است، لطفا چند روز دیگر مجدد برای انتقال تلاش کنید.</p>
                                <p>Alternatively, you can perform a search to register this domain.</p>
                            </div>
                            <div class="domain-invalid">
                                <p class="domain-checker-unavailable headline">نام دامنه وارد شده، نامعتبر است</p>
                                <p>
                                    Domains must begin with a letter or a number<span class="domain-length-restrictions"> and be between <span class="min-length"></span> and <span class="max-length"></span> characters in length</span><br />
                                    لطفا مقادیر وارد شده را مجدد بررسی کنید
                                </p>
                            </div>
                            <div class="domain-price">
                                <span class="register-price-label">ادامه جهت ثبت دامنه برای</span>
                                <span class="transfer-price-label hidden">Transfer to us and extend by 1 year* for</span>
                                <span class="price"></span>
                            </div>
                            <input type="hidden" id="resultDomainOption" name="domainoption" />
                            <input type="hidden" id="resultDomain" name="domains[]" />
                            <input type="hidden" id="resultDomainPricingTerm" />
                        </div>
                    </div>

                                            
                        <div class="suggested-domains hidden">
                            <div class="panel-heading">
                                نام های پیشنهادی
                            </div>
                            <div id="suggestionsLoader" class="panel-body domain-lookup-loader domain-lookup-suggestions-loader">
                                <i class="fa fa-spinner fa-spin"></i> در حال ایجاد لیست پیشنهادی برای شما
                            </div>
                            <ul id="domainSuggestions" class="domain-lookup-result list-group hidden">
                                <li class="domain-suggestion list-group-item hidden">
                                    <span class="domain"></span><span class="extension"></span>
                                    <button type="button" class="btn btn-add-to-cart product-domain" data-whois="1" data-domain="">
                                        <span class="to-add">افزودن به کارت</span>
                                        <span class="added">افزوده شده</span>
                                        <span class="unavailable">تکراری</span>
                                    </button>
                                    <button type="button" class="btn btn-primary domain-contact-support hidden">Contact Support to Purchase</button>
                                    <span class="price"></span>
                                    <span class="promo hidden"></span>
                                </li>
                            </ul>
                            <div class="panel-footer more-suggestions hidden text-center">
                                <a id="moreSuggestions" href="#" onclick="loadMoreSuggestions();return false;">پیشنهادات دیگری میخواهم.</a>
                                <span id="noMoreSuggestions" class="no-more small hidden">کل اطلاعاتی که داریم همین است.اگر همچنان به دنبال موردی هستید، لطفا با کلمه کلیدی دیگری امتحان کنید</span>
                            </div>
                            <div class="text-center text-muted domain-suggestions-warning">
                                <p>پیشنهاد نام های دیگر به معنای در دسترس بودن قطعی این نام ها و امکان ثبت برای شما نمیباشد زیرا بررسی در دسترس بودن دامنه ها به صورت لحظه ای انجام میشود</p>
                            </div>
                        </div>
                                    </div>

                <div class="text-center">
                    <button id="btnDomainContinue" type="submit" class="btn btn-primary btn-lg hidden" disabled="disabled">
                        ادامه
                        &nbsp;<i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<p style="text-align:center;">Powered by <a href="http://www.whmcs.com/" target="_blank">WHMCompleteSolution</a></p>


        </div><!-- /.main-content -->
            </div>
    <div class="clearfix"></div>
</section>




<!-- ======= Guaratess ======= -->
    <div class="guaratess">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 guarate m-b-mob-30">
                    <img src="templates/grehost/img/guaratess/guaratess-1.png" alt="">
                    <p></p>
                </div>
                <div class="col-sm-3 guarate m-b-mob-30">
                    <img src="templates/grehost/img/guaratess/guaratess-2.png" alt="">
                    <p></p>
                </div>
                <div class="col-sm-3 guarate m-b-mob-30">
                    <img src="templates/grehost/img/guaratess/guaratess-3.png" alt="">
                    <p></p>
                </div>
                <div class="col-sm-3 guarate">
                    <img src="templates/grehost/img/guaratess/guaratess-4.png" alt="">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <!-- ======= Footer ======= -->
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-3 m-b-mob-30 m-b-tab-30">
                    <h4 class="footer-title">About Us</h4>
                    <p>Amet sodales scelerisque dolor ipsum vitae. Neque leo, id luctus nunc quisquam, dolor venenatis auctor morbi. Amet nullam, velit integer at, praesent fermentum, sed donec congue ut luctus ut mi. Scelerisque a lorem amet, id litora ullamcorper pede cras aliquid.</p>
                    <ul class="list-unstyled list-inline footer-social">
                        <li><a href="#" class="facebook"><i class="icon ion-social-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="icon ion-social-twitter"></i></a></li>
                        <li><a href="#" class="googleplus"><i class="icon ion-social-googleplus"></i></a></li>
                        <li><a href="#" class="pinterest"><i class="icon ion-social-pinterest"></i></a></li>
                        <li><a href="#" class="dribbble"><i class="icon ion-social-dribbble-outline"></i></a></li>
                        <li><a href="#" class="tumblr"><i class="icon ion-social-tumblr"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-lg-3 m-b-mob-30 m-b-tab-30">
                    <h4 class="footer-title"></h4>
                    <ul class="list-unstyled">
                        <li><a href="index-3.html">ناحیه کاربری</a></li>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="blog.html"></a></li>
                        <li><a href="portfolio.html"></a></li>
                        <li><a href="contact-2.html" class="no-border">تماس با ما</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-lg-3 m-b-mob-30">
                    <h4 class="footer-title">سرویس ها</h4>
                    <ul class="list-unstyled">
                        <li><a href="hosting-shared.html">Shared Hosting</a></li>
                        <li><a href="hosting-cloud.html">Cloud Hosting</a></li>
                        <li><a href="hosting-dedicated.html">Dedicated Hosting</a></li>
                        <li><a href="features.html">Hosting Features</a></li>
                        <li><a href="domain.html" class="no-border">Domains Names</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <h4 class="footer-title">Newsletter</h4>
                    <p></p>
                    <form method="post">
<input type="hidden" name="token" value="a7ee79e38a77c41780c55d6a594f65d4ad2275b5" />
                        <div class="form-group">
                            <input type="text" class="form-control" id="subscribe-name" placeholder="نام">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control email" id="subscribe-email" placeholder=" آدرس ایمیل">
                        </div>
                        <button type="submit" class="btn">ارسال پیام</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= Copyrights ======= -->
    <div class="copyright">
        <div class="container">
            <p>تمام حقوق محفوظ است <a href="index-3.html" class="main-color"></a></p>
            <a role="button" class="main-color up"><img src="templates/grehost/img/up.png" alt=""></a>
        </div>
    </div>



</body>

<!-- Mirrored from hananhamdy.com/projects/clients/cart.php?a=add&pid=3&language=farsi by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Oct 2018 06:35:53 GMT -->
</html>
